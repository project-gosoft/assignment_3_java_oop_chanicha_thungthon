import java.util.ArrayList;
import java.util.Scanner;

public class remove {

	static Scanner scan = new Scanner(System.in);

	public static void main(String[] args) {
		// Integer ArrayList
		ArrayList<Integer> com = new ArrayList<Integer>();

		com.add(1);
		com.add(2);
		com.add(3);
		com.add(4);
		com.add(5);
		com.add(6);

		System.out.print("ArrayList before remove:");

		for (Integer var : com) {
			System.out.printf("[%-1d],",var);
		}
		System.out.print("\nInput id com: "); // รับค่า id com ที่จะลบออก
		int id = scan.nextInt();
		for (int i = 0; i <= com.size(); i++) {
			if (id == i) {
				com.remove(i - 1);
				System.out.println("Report");
				System.out.println("ArrayList id remove: " + id);
				System.out.println("ArrayList After remove:" + com);
			}
		}
	}
}
